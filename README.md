# Authelia-nginx
Conjunto de archivos de configuración para utilizar authelia como backend de autenticación para nginx


1. git clone
2. ajustar configuración en authelia/configuration.yml
3. ajustar usuarios en authelia/users_database.yml
4. copiar contenido de carpeta nginx/conf.d en /etc/nginx/conf.d
5. ajustar configuración nginx
